const button = document.querySelector('.button')
const information = document.querySelector('#result')

async function getIP(ip) {
    const response = await fetch(ip)
    const result = await response.json()
    return result
}

async function findByIP() {
    const ipResult = await getIP('http://api.ipify.org/?format=json')
    const ip = ipResult.ip
    
    const locationResult = await getIP(`http://ip-api.com/json/${ip}`)
    const continent = locationResult.timezone.split('/')[0]
    const country = locationResult.country
    const region = locationResult.region
    const city = locationResult.city
    const district = locationResult.regionName

    information.innerHTML = `<hr> Continent: ${continent} <hr> Country: ${country} <hr> Region: ${region} <hr> City: ${city} <hr> District: ${district} <hr>`
}

button.addEventListener('click', () => {
    findByIP()
})